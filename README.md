# eXceed12 LinkZeed: Smart FireFighter #

* From: Kasetsart University, Thailand
* Theme: Hero
* Main Topic: Internet of Things
* Wiki: http://exceed.cpe.ku.ac.th/wiki/index.php/Exceed_12_LinkZeed
* This Repository contain both code for Intel Galileo Gen 2 and Node MCU for our group.

### Detial ###

* "Intel Galielo Gen 2 - LinkZeed - Smart FireFighter.ino" : Source code for Intel Galileo Gen 2
* "Node MCU - LinkZeed.ino" : Source code for Node MCU