#include<LiquidCrystal.h>
#include<pt.h>

#define PT_DELAY( pt, ms, ts ) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));

// Sensor defining
#define MQ5 A5
#define IR A4
#define redLED 11
#define greenLED 12
#define blueLED 13
#define buzzer 10
#define alertLED A0
#define resetSTATEsw 2

// STATE
// Status of respon
#define NOFIREMAN 0
#define FIREMANON 1
#define FIREOUT 2
#define NORMALRESPON 3
int responSTATE = NORMALRESPON;

// Data chanel
//#define SERVER 0
//#define SENSOR 1
//int dataSTATE;

// Status of Sensor
#define WAITFORALERT 0
#define DETECTEDFIRE 1
#define WATERON 2
#define READYFORRECOVERY 3
int sensorSTATE = WAITFORALERT;

// Status of fire
#define NORMAL 0
#define FIRED 1
int fireSTATE = NORMAL;

// Sensor alarm
#define alarmOFF 0
#define alarmON 1

int gasAlarm = alarmOFF;
int irAlarm = alarmOFF;

// struct
struct pt pt_taskGas;
struct pt pt_taskIR;
struct pt pt_taskRGB;
struct pt pt_taskResetStateSw;
struct pt pt_taskSendSerial;
struct pt pt_taskExtra;

// Initial LCD
LiquidCrystal lcd( 8, 9, 4, 5, 6, 7 );

// Sensor detial
String room = "401";

// Limit
#define gasLimit 400
#define irLimit 400

// Other word
String normal = "NORMAL";
String fire = "FIRE";
String gasMsg = "";
String irMsg = "";
int detectedInterval;
int pushSW;
bool passState2 = false;

// online method
String recievedData = "";
void serialEvent() {
   if (Serial1.available() > 0) {
     recievedData = Serial1.readStringUntil('\r');
     Serial.println("Recieve : " + recievedData);
     setValue();
   }
   
}

String sendData = "";
void sendSerial() {
   Serial1.print( responSTATE );
   Serial1.print( "\r" );
   Serial.println( "Send: " + responSTATE );
}

void setValue() {
   if ( recievedData == "1") {
      responSTATE = FIREMANON;
   } else if ( recievedData.charAt( 0 ) == '2' ) {
      responSTATE = FIREOUT;
      passState2 = true;
   } else if ( recievedData.charAt( 0 ) == '3' && passState2 ) {
      responSTATE = NORMALRESPON;
      passState2 = false;
   }
}

void initPinMode() {
   pinMode( MQ5, INPUT );
   pinMode( IR, INPUT );
   pinMode( resetSTATEsw, INPUT );
   pinMode( buzzer, OUTPUT );
   pinMode( redLED, OUTPUT );
   pinMode( greenLED, OUTPUT );
   pinMode( blueLED, OUTPUT );
   pinMode( alertLED, OUTPUT );
}

// Working method
void rgbLight() {
  digitalWrite( redLED, LOW );
  digitalWrite( greenLED, LOW );
  digitalWrite( blueLED, LOW );
   if ( responSTATE == NOFIREMAN ) {
      digitalWrite( blueLED, HIGH );
   } else if ( responSTATE == FIREMANON ) {
      digitalWrite( greenLED, HIGH );
      digitalWrite( redLED, HIGH );
   } else if ( responSTATE == NORMALRESPON ) {
      digitalWrite( greenLED, HIGH );
   }
}

void printLcd() {

   lcd.clear();
   lcd.begin( 16, 2 );
   lcd.setCursor( 0, 0 );
   lcd.print( "Gas" );
   lcd.setCursor( 7, 0 );
   lcd.print( "IR" );

   lcd.setCursor( 0, 1 );
   lcd.print( gasMsg );
   lcd.setCursor( 7, 1 );
   lcd.print( irMsg );

}

void alarm() {
   Serial.print( "gas: " );
   Serial.println( gasAlarm );
   Serial.print( "ir: " );
   Serial.println( irAlarm );
   if ( gasAlarm == alarmON || irAlarm == alarmON ) {
      analogWrite( buzzer, 100 );
      digitalWrite( alertLED, 1 );
   } else {
      analogWrite( buzzer, 0 );
      digitalWrite( alertLED, 0 );
   }
}

// Thread
// Gas
int gasVolume;
PT_THREAD( taskGas( struct pt* pt ) ) {
   static uint32_t ts;
   PT_BEGIN( pt );
   while ( 1 ) {
      gasVolume = analogRead( MQ5 );
      if ( gasVolume > gasLimit ) {
         gasMsg = fire;
         gasAlarm = alarmON;
         responSTATE = NOFIREMAN;
         passState2 = false;
      } else {
         gasMsg = normal;
         gasAlarm = alarmOFF;
      }
      PT_DELAY( pt, 150, ts );
   }
   PT_END( pt );
}
// IR

int irVolume;
PT_THREAD( taskIR( struct pt* pt ) ) {
   static uint32_t ts;
   PT_BEGIN( pt );
   while ( 1 ) {
      irVolume = analogRead( IR );
      if ( irVolume < irLimit ) {
         irMsg = normal;
         irAlarm = alarmON;
         responSTATE = NOFIREMAN;
         passState2 = false;
      } else {
         irMsg = normal;
         irAlarm = alarmOFF;
      }
      PT_DELAY( pt, 150, ts );
   }
   PT_END( pt );
}
// RGB
PT_THREAD( taskRGB( struct pt* pt ) ) {
   static uint32_t ts;
   PT_BEGIN( pt );
   while ( 1 ) {
      detectedInterval = 0;
      if ( fireSTATE == FIRED ) {
         sensorSTATE = DETECTEDFIRE;
         responSTATE = NOFIREMAN;
         passState2 = false;
      } else if ( sensorSTATE == DETECTEDFIRE ) {
         if ( detectedInterval > 5 ) {
            sensorSTATE = WATERON;
         } else {
            detectedInterval++;
         }
      } else if ( sensorSTATE == WATERON ) {
         if ( responSTATE == FIREOUT && passState2) {
            sensorSTATE = READYFORRECOVERY;
            responSTATE = NORMALRESPON;
         }
      }
      rgbLight();
      PT_DELAY( pt, 150, ts );
   }
   PT_END( pt );
}
// Reset STATE switch
PT_THREAD( taskResetStateSw( struct pt* pt ) ) {
   static uint32_t ts;
   PT_BEGIN( pt );
   while ( 1 ) {
      pushSW = digitalRead( resetSTATEsw );
      if ( pushSW == 0 ) {
         fireSTATE = NORMAL;
      }
      PT_DELAY( pt, 150, ts );
   }
   PT_END( pt );
}
// Send Serial
PT_THREAD( taskSendSerial( struct pt* pt ) ) {
   static uint32_t ts;
   PT_BEGIN( pt );
   while ( 1 ) {
      sendSerial();
      PT_DELAY( pt, 600, ts );
   }
   PT_END( pt );
}

// Extra
PT_THREAD( taskExtra( struct pt* pt ) ) {
   static uint32_t ts;
   PT_BEGIN( pt );
   while ( 1 ) {
     printLcd();
     alarm();
     PT_DELAY( pt, 200, ts );
   }
   PT_END( pt );
}

// Board control
void setup() {

   Serial.begin( 115200 );
   Serial1.begin( 115200 );

   Serial.println( "test" );
   
   Serial.println( "test" );
   initPinMode();

   lcd.begin( 16, 2 );
   
   PT_INIT( &pt_taskGas );
   PT_INIT( &pt_taskIR );
   PT_INIT( &pt_taskRGB );
   PT_INIT( &pt_taskResetStateSw );
   PT_INIT( &pt_taskSendSerial );
}

void loop() {
   taskGas( &pt_taskGas );
   taskIR( &pt_taskIR );
   taskRGB( &pt_taskRGB );
   taskResetStateSw( &pt_taskResetStateSw );
   taskExtra( &pt_taskExtra );
   
   serialEvent();
   taskSendSerial( &pt_taskSendSerial );
}
